﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WatchfulEyE;
using Database;

public partial class _Default : Page
{
    TwitterStatusesAdapter statusesAdapter = new TwitterStatusesAdapter();
    TriplesAdapter triplesAdapter = new TriplesAdapter();

    protected void Page_Load(object sender, EventArgs e)
    { 
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

        //   twitter();
        analysis();
    }

    private void twitter()
    {
        TextBox1.Text = Data.Value.ToString();
        List<string> Points = TextBox1.Text.Split('_').ToList();
        List<TwitterStatuse> statuses = new List<TwitterStatuse>();

        string q = "";
        string result_type = "mixed";
        string count = "100";
        string lang = "en";

        foreach (string point in Points)
        {
            if (point != "")
            {
                string geocode = point.TrimStart(',');
                float longitude = Convert.ToSingle(geocode.Split(',')[0]);
                float latitude = Convert.ToSingle(geocode.Split(',')[1]); ;
                geocode = geocode.Replace(",", "%2C");
                geocode += "%2C1km";

                string since = from.Value.ToString();
                string until = to.Value.ToString();
                StatuseFactory factory = new StatuseFactory(q, geocode, since, until, result_type, count, lang);
                statuses = factory.Fact();


                foreach (TwitterStatuse statuse in statuses)
                {
                    statuse.longitude = longitude;
                    statuse.latitude = latitude;
                    Label1.Text += "\n\ntweet: " + statuse.tweet + "\n\ndate: " + statuse.createdDate + "\n\ngeo:"
                          + statuse.longitude + statuse.latitude;
                    statuse.tweet = SlangToEng.WhiteList(statuse.tweet);
                    statusesAdapter.SetTwitterStatuse(statuse);
                }
            }
        }
    }
    private void analysis()
    {
    /*    List<SPO> spos;
        List<TwitterStatuse> statuses = new List<TwitterStatuse>();
        statuses = statusesAdapter.GetAllTwitterStatuse();
        Triple triple;
        List<Triple> triples = new List<Triple>();
        foreach (TwitterStatuse statuse in statuses)
        {

            spos = StanfordDependencies.getTriples(statuse.tweet);
            foreach (SPO spo in spos)
            {
                triple = new Triple();
                if (spo.Subject.Equals(String.Empty) ||
                    spo.Predicted.Equals(String.Empty) ||
                    spo.Object.Equals(String.Empty) ||
                    spo.Subject.Contains("http") ||
                    spo.Predicted.Contains("http") ||
                    spo.Object.Contains("http"))
                    continue;

                string sbj = spo.Subject;
                triple.subjectt = sbj;
                triple.subjectURI = RDFCreator.we_ns + sbj;

                string prd = spo.Predicted;
                triple.predicted = prd;
                triple.predictedURI = RDFCreator.we_ns + prd;

                string obj = spo.Object;
                triple.objectt = obj;
                triple.objectURI = RDFCreator.we_ns + obj;

                triple.statuseId = statuse.statuseId;

                Label1.Text += " subject: " + triple.subjectt +
                    " predicated: " + triple.predicted +
                    " object: " + triple.objectt;
                triplesAdapter.SetTriple(triple);
                triples.Add(triple);
            }
        }*/

        RDFCreator creator = new RDFCreator();
        /* IGraph graph = new Graph();

         foreach (Database.Triple _triple in triplesAdapter.GetAllTriples())
         { 
             IUriNode su = graph.CreateUriNode(UriFactory.Create(_triple.subjectt));
             IUriNode pr = graph.CreateUriNode(UriFactory.Create(_triple.Verb));
             ILiteralNode ob = graph.CreateLiteralNode(_triple.Obj + "_" + sen.StartTime);
             graph.Assert(new Triple(su, pr, ob));
         }*/
        creator.Create(triplesAdapter.GetAllTriples());
        Label1.Text += "" + creator.Validate();
    }
}