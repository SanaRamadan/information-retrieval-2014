﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Demo Page: Using Progressive Enhancement to Convert a Select Box Into an Accessible jQuery UI Slider</title>
    <script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.7.1.custom.min.js"></script>
    <script type="text/javascript" src="js/selectToUISlider.jQuery.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
    <link rel="stylesheet" href="css/redmond/jquery-ui-1.7.1.custom.css" type="text/css" />
    <link rel="Stylesheet" href="css/ui.slider.extras.css" type="text/css" />
    <style type="text/css">
        html, body, #map-canvas
        {
            font-size: 70.5%;
            font-family: "Segoe UI","Helvetica Neue",Helvetica,Arial,sans-serif;
            height: 100%;
            margin: 0px;
            padding: 0px;
        }

        fieldset
        {
            border: 0;
            margin: 6em;
            height: 12em;
        }

        label
        {
            font-weight: normal;
            float: left;
            margin-right: .5em;
            font-size: 1.1em;
        }

        select
        {
            margin-right: 1em;
            float: left;
        }

        .ui-slider
        {
            clear: both;
            top: 5em;
        }

        .controls
        {
            margin-top: 16px;
            border: 1px solid transparent;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            height: 32px;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        }

        #pac-input
        {
            background-color: #fff;
            padding: 0 11px 0 13px;
            width: 400px;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            text-overflow: ellipsis;
        }

            #pac-input:focus
            {
                border-color: #4d90fe;
                margin-left: -1px;
                padding-left: 14px; /* Regular padding-left + 1. */
                width: 401px;
            }

        .pac-container
        {
            font-family: Roboto;
        }

        #type-selector
        {
            color: #fff;
            background-color: #4d90fe;
            padding: 5px 11px 0px 11px;
        }

            #type-selector label
            {
                font-family: Roboto;
                font-size: 13px;
                font-weight: 300;
            }
    </style>
    <script type="text/javascript">
        $(function () {
            $('select#valueAA, select#valueBB').selectToUISlider({
                labels: 12
            });

            //fix color 
            fixToolTipColor();
        });
        function fixToolTipColor() {
            //grab the bg color from the tooltip content - set top border of pointer to same
            $('.ui-tooltip-pointer-down-inner').each(function () {
                var bWidth = $('.ui-tooltip-pointer-down-inner').css('borderTopWidth');
                var bColor = $(this).parents('.ui-slider-tooltip').css('backgroundColor')
                $(this).css('border-top', bWidth + ' solid ' + bColor);
            });
        }
    </script>

    <script type="text/javascript">
        var poly, map;
        var markers = [];
        var points = [];
        var path = new google.maps.MVCArray;
        var infoWindow;

        function HomeControl(controlDiv, map) {

            // Set CSS styles for the DIV containing the control
            // Setting padding to 5 px will offset the control
            // from the edge of the map
            controlDiv.style.padding = '5px';

            // Set CSS for the control border
            var controlUI = document.createElement('div');
            controlUI.style.backgroundColor = 'white';
            controlUI.style.cursor = 'pointer';
            controlUI.style.textAlign = 'center';
            controlUI.title = 'Click if you have finished markering';
            controlDiv.appendChild(controlUI);

            // Set CSS for the control interior
            var controlText = document.createElement('div');
            controlText.style.fontFamily = 'Arial,sans-serif';
            controlText.style.fontSize = '12px';
            controlText.style.paddingLeft = '4px';
            controlText.style.paddingRight = '4px';
            controlText.innerHTML = '<b>Submit</b>';
            controlUI.appendChild(controlText);

            // Setup the click event listeners: simply set the map to
            // Chicago
            google.maps.event.addDomListener(controlUI, 'click', function () {
                savePoints();
            });

        }

        function savePoints() {
            var max = FindMaxLat();
            var res = max.split(",");
            var Lat = parseFloat(res[0]);
            var Lng = parseFloat(res[1]);
            var i = 0;
            while (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(Lat, Lng), poly) == true) {
                Lng += 0.00000000001;
                if (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(Lat, Lng), poly) == true) {
                    while (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(Lat, Lng), poly) == true) {
                        points[i] = Lat + ',' + Lng + '_';
                        i++;
                        Lng += 0.00000000001;
                    }
                }
                else {
                    Lng -= 0.00000000001;
                    if (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(Lat, Lng), poly) == true)
                        while (google.maps.geometry.poly.containsLocation(new google.maps.LatLng(Lat, Lng), poly) == true) {
                            points[i] = Lat + ',' + Lng + '_';
                            i++;
                            Lng -= 0.00000000001;
                        }
                    else
                        Lng += 0.00000000001;
                }


                Lat += 0.00000000001;
            }
            document.getElementById("Data").value = points;
            document.getElementById("from").value = valueAA.value;
            document.getElementById("to").value = valueBB.value;
        }


        function FindMaxLat() {
            var polygonBounds = poly.getPath();
            var maxLat = 0;
            var Lng = polygonBounds.getAt(0).lng();
            var coordinates = [];
            for (var i = 0 ; i < polygonBounds.length ; i++) {
                if (polygonBounds.getAt(i).lat() > maxLat) {
                    maxLat = polygonBounds.getAt(i).lat();
                    Lng = polygonBounds.getAt(i).lng();
                }
            }
            var max = maxLat + ',' + Lng;
            // for (var i = 0 ; i < coordinates.length ; i++) {
            // if (coordinates[i] > max)
            //max = coordinates[i];
            //  }
            return max;
        }


        function initialize() {
            var uluru = new google.maps.LatLng(55.344, 36.036);
            map = new google.maps.Map(document.getElementById("map"), {
                zoom: 9,
                center: uluru

            });

            poly = new google.maps.Polygon({
                strokeWeight: 3,
                fillColor: '#5555FF'
            });
            poly.setMap(map);
            poly.setPaths(new google.maps.MVCArray([path]));

            infoWindow = new google.maps.InfoWindow();

            var homeControlDiv = document.createElement('div');
            var homeControl = new HomeControl(homeControlDiv, map);

            homeControlDiv.index = 1;
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(homeControlDiv);

            google.maps.event.addListener(map, 'click', addPoint);

            var input = /** @type {HTMLInputElement} */(
               document.getElementById("pac-input"));
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            var searchBox = new google.maps.places.SearchBox(
              /** @type {HTMLInputElement} */(input));

            // [START region_getplaces]
            // Listen for the event fired when the user selects an item from the
            // pick list. Retrieve the matching places for that item.
            google.maps.event.addListener(searchBox, 'places_changed', function () {
                var places = searchBox.getPlaces();

                for (var i = 0, marker; marker = markers[i]; i++) {
                    marker.setMap(null);
                }

                // For each place, get the icon, place name, and location.
                markers = [];
                var bounds = new google.maps.LatLngBounds();
                for (var i = 0, place; place = places[i]; i++) {
                    var image = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };
                    // Create a marker for each place.
                    var marker = new google.maps.Marker({
                        map: map,
                        icon: image,
                        title: place.name,
                        position: place.geometry.location
                    });

                    markers.push(marker);

                    bounds.extend(place.geometry.location);
                }

                map.fitBounds(bounds);
            });
            // [END region_getplaces]

        }



        function addPoint(event) {
            path.insertAt(path.length, event.latLng);

            var marker = new google.maps.Marker({
                position: event.latLng,
                map: map,
                draggable: true
            });
            markers.push(marker);
            marker.setTitle("#" + path.length);

            var ne = event.latLng;
            var contentString = ne.lat() + ',' + ne.lng() + '_';
            //  points.push(contentString);
            infoWindow.setContent(contentString);
            infoWindow.setPosition(ne);

            infoWindow.open(map);


            google.maps.event.addListener(marker, 'click', function () {
                marker.setMap(null);
                for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
                markers.splice(i, 1);
                //  points.splice(i,1);
                path.removeAt(i);
            }
            );
            google.maps.event.addListener(marker, 'dragend', function () {
                for (var i = 0, I = markers.length; i < I && markers[i] != marker; ++i);
                path.setAt(i, marker.getPosition());
                var ne = marker.getPosition();
                var contentString = ne.lat() + ',' + ne.lng() + '_';
                infoWindow.setContent(contentString);
                infoWindow.setPosition(ne);
                //  points[i] = contentString;
                //alert(points[i]);
            }
            );
        }
    </script>



</head>
<body>
    <div>
        <asp:Label ID="Label1" runat="server" />
        <input id="pac-input" name="pac-input" class="controls" type="text" />
        <label for="valueAA">From:</label>
        <select name="valueAA" id="valueAA">
            <optgroup label="2008">
                <option value="2008-01-01">Jan 08</option>
                <option value="2008-02-01">Feb 08</option>
                <option value="2008-03-01">Mar 08</option>
                <option value="2008-04-01">Apr 08</option>
                <option value="2008-05-01">May 08</option>
                <option value="2008-06-01">Jun 08</option>
                <option value="2008-07-01">Jul 08</option>
                <option value="2008-08-01">Aug 08</option>
                <option value="2008-09-01">Sep 08</option>
                <option value="2008-10-01">Oct 08</option>
                <option value="2008-11-01">Nov 08</option>
                <option value="2008-12-01">Dec 08</option>
            </optgroup>
            <optgroup label="2009">
                <option value="2009-01-01">Jan 09</option>
                <option value="2009-02-01">Feb 09</option>
                <option value="2009-03-01">Mar 09</option>
                <option value="2009-04-01">Apr 09</option>
                <option value="2009-05-01">May 09</option>
                <option value="2009-06-01">Jun 09</option>
                <option value="2009-07-01">Jul 09</option>
                <option value="2009-08-01">Aug 09</option>
                <option value="2009-09-01">Sep 09</option>
                <option value="2009-10-01">Oct 09</option>
                <option value="2009-11-01">Nov 09</option>
                <option value="2009-12-01">Dec 09</option>
            </optgroup>
            <optgroup label="2010">
                <option value="2010-01-01">Jan 10</option>
                <option value="2010-02-01">Feb 10</option>
                <option value="2010-03-01">Mar 10</option>
                <option value="2010-04-01">Apr 10</option>
                <option value="2010-05-01">May 10</option>
                <option value="2010-06-01">Jun 10</option>
                <option value="2010-07-01">Jul 10</option>
                <option value="2010-08-01">Aug 10</option>
                <option value="2010-09-01">Sep 10</option>
                <option value="2010-10-01">Oct 10</option>
                <option value="2010-11-01">Nov 10</option>
                <option value="2010-12-01">Dec 10</option>
            </optgroup>
            <optgroup label="2011">
                <option value="2011-01-01">Jan 11</option>
                <option value="2011-02-01">Feb 11</option>
                <option value="2011-03-01">Mar 11</option>
                <option value="2011-04-01">Apr 11</option>
                <option value="2011-05-01">May 11</option>
                <option value="2011-06-01">Jun 11</option>
                <option value="2011-07-01">Jul 11</option>
                <option value="2011-08-01">Aug 11</option>
                <option value="2011-09-01">Sep 11</option>
                <option value="2011-10-01">Oct 11</option>
                <option value="2011-11-01">Nov 11</option>
                <option value="2011-12-01">Dec 11</option>
            </optgroup>
            <optgroup label="2012">
                <option value="2012-01-01">Jan 02</option>
                <option value="2012-02-01">Feb 02</option>
                <option value="2012-03-01">Mar 02</option>
                <option value="2012-04-01">Apr 02</option>
                <option value="2012-05-01">May 02</option>
                <option value="2012-06-01">Jun 02</option>
                <option value="2012-07-01">Jul 02</option>
                <option value="2012-08-01">Aug 02</option>
                <option value="2012-09-01">Sep 02</option>
                <option value="2012-10-01">Oct 02</option>
                <option value="2012-11-01">Nov 02</option>
                <option value="2012-12-01">Dec 02</option>
            </optgroup>
            <optgroup label="2013">
                <option value="2013-01-01">Jan 03</option>
                <option value="2013-02-01">Feb 03</option>
                <option value="2013-03-01">Mar 03</option>
                <option value="2013-04-01">Apr 03</option>
                <option value="2013-05-01">May 03</option>
                <option value="2013-06-01">Jun 03</option>
                <option value="2013-07-01">Jul 03</option>
                <option value="2013-08-01">Aug 03</option>
                <option value="2013-09-01">Sep 03</option>
                <option value="2013-10-01">Oct 03</option>
                <option value="2013-11-01">Nov 03</option>
                <option value="2013-12-01">Dec 03</option>
            </optgroup>
            <optgroup label="2014">
                <option value="2014-01-01">Jan 04</option>
                <option value="2014-02-01">Feb 04</option>
                <option value="2014-03-01">Mar 04</option>
                <option value="2014-04-01" selected="selected">Apr 01</option>
            </optgroup>
        </select>

        <label for="valueBB">To:</label>
        <select name="valueBB" id="valueBB">
            <optgroup label="2008">
                <option value="2008-01-01">Jan 08</option>
                <option value="2008-02-01">Feb 08</option>
                <option value="2008-03-01">Mar 08</option>
                <option value="2008-04-01">Apr 08</option>
                <option value="2008-05-01">May 08</option>
                <option value="2008-06-01">Jun 08</option>
                <option value="2008-07-01">Jul 08</option>
                <option value="2008-08-01">Aug 08</option>
                <option value="2008-09-01">Sep 08</option>
                <option value="2008-10-01">Oct 08</option>
                <option value="2008-11-01">Nov 08</option>
                <option value="2008-12-01">Dec 08</option>
            </optgroup>
            <optgroup label="2009">
                <option value="2009-01-01">Jan 09</option>
                <option value="2009-02-01">Feb 09</option>
                <option value="2009-03-01">Mar 09</option>
                <option value="2009-04-01">Apr 09</option>
                <option value="2009-05-01">May 09</option>
                <option value="2009-06-01">Jun 09</option>
                <option value="2009-07-01">Jul 09</option>
                <option value="2009-08-01">Aug 09</option>
                <option value="2009-09-01">Sep 09</option>
                <option value="2009-10-01">Oct 09</option>
                <option value="2009-11-01">Nov 09</option>
                <option value="2009-12-01">Dec 09</option>
            </optgroup>
            <optgroup label="2010">
                <option value="2010-01-01">Jan 10</option>
                <option value="2010-02-01">Feb 10</option>
                <option value="2010-03-01">Mar 10</option>
                <option value="2010-04-01">Apr 10</option>
                <option value="2010-05-01">May 10</option>
                <option value="2010-06-01">Jun 10</option>
                <option value="2010-07-01">Jul 10</option>
                <option value="2010-08-01">Aug 10</option>
                <option value="2010-09-01">Sep 10</option>
                <option value="2010-10-01">Oct 10</option>
                <option value="2010-11-01">Nov 10</option>
                <option value="2010-12-01">Dec 10</option>
            </optgroup>
            <optgroup label="2011">
                <option value="2011-01-01">Jan 11</option>
                <option value="2011-02-01">Feb 11</option>
                <option value="2011-03-01">Mar 11</option>
                <option value="2011-04-01">Apr 11</option>
                <option value="2011-05-01">May 11</option>
                <option value="2011-06-01">Jun 11</option>
                <option value="2011-07-01">Jul 11</option>
                <option value="2011-08-01">Aug 11</option>
                <option value="2011-09-01">Sep 11</option>
                <option value="2011-10-01">Oct 11</option>
                <option value="2011-11-01">Nov 11</option>
                <option value="2011-12-01">Dec 11</option>
            </optgroup>
            <optgroup label="2012">
                <option value="2012-01-01">Jan 02</option>
                <option value="2012-02-01">Feb 02</option>
                <option value="2012-03-01">Mar 02</option>
                <option value="2012-04-01">Apr 02</option>
                <option value="2012-05-01">May 02</option>
                <option value="2012-06-01">Jun 02</option>
                <option value="2012-07-01">Jul 02</option>
                <option value="2012-08-01">Aug 02</option>
                <option value="2012-09-01">Sep 02</option>
                <option value="2012-10-01">Oct 02</option>
                <option value="2012-11-01">Nov 02</option>
                <option value="2012-12-01">Dec 02</option>
            </optgroup>
            <optgroup label="2013">
                <option value="2013-01-01">Jan 03</option>
                <option value="2013-02-01">Feb 03</option>
                <option value="2013-03-01">Mar 03</option>
                <option value="2013-04-01">Apr 03</option>
                <option value="2013-05-01">May 03</option>
                <option value="2013-06-01">Jun 03</option>
                <option value="2013-07-01">Jul 03</option>
                <option value="2013-08-01">Aug 03</option>
                <option value="2013-09-01">Sep 03</option>
                <option value="2013-10-01">Oct 03</option>
                <option value="2013-11-01">Nov 03</option>
                <option value="2013-12-01">Dec 03</option>
            </optgroup>
            <optgroup label="2014">
                <option value="2014-01-01">Jan 04</option>
                <option value="2014-02-01">Feb 04</option>
                <option value="2014-03-01">Mar 04</option>
                <option value="2014-04-01" selected="selected">Apr 01</option>
            </optgroup>
        </select>

    </div>

    <div id="Div1" runat="server">
        <input type="hidden" value="" id="SendA" name="SendA" />
        <form id="Form1" runat="server">
            <asp:HiddenField ID="Data" runat="server" />
            <asp:HiddenField ID="from" runat="server" />
            <asp:HiddenField ID="to" runat="server" />
            <asp:TextBox ID="TextBox1" runat="server" Text="from" />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Search" />
        </form>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </div>
    <div id="map" style="height: 480px; width: 1000px;" />


    <script type="text/javascript">initialize();</script>

</body>
</html>
