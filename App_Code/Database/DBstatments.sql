
create database WatchfulEyE

create table TwitterStatuses( 
statuseId bigint primary key,
tweet varchar(140) not null,
longitude float not null,
latitude  float not null,
createdDate datetime not null
)

create table Triples(
tripleId bigint identity(1,1) primary key,
statuseId bigint  not null,
subjectt  varchar(50)  not null,
subjectURI  varchar(150)  not null,
predicted varchar(50)  not null, 
predictedURI varchar(150)  not null, 
objectt varchar(50)  not null,
objectURI varchar(150)  not null, 

foreign key (statuseId) references TwitterStatuses (statuseId) on delete cascade
)