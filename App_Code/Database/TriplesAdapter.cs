﻿namespace WatchfulEyE
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Database;

    /// <summary>
    /// Summary description for TriplesAdapter
    /// </summary>
    public class TriplesAdapter
    {
        private DBEntityDataContext db = new DBEntityDataContext();

        public TriplesAdapter()
        {

        }
        public List<Database.Triple> GetAllTriples()
        {
            var statusesQuery = (from s in db.Triples
                                 select s);

            List<Database.Triple> triples = new List<Database.Triple>();
            triples = statusesQuery.ToList<Database.Triple>();

            return triples;
        }

        public void SetTriple(Database.Triple triple)
        {
            db.Triples.InsertOnSubmit(triple);
            db.SubmitChanges();
        }

        public void UpdateTriple(Database.Triple triple,
            string subjectURI, string objectURI, string predictedURI)
        {
            triple.subjectURI = subjectURI;
            triple.objectURI = objectURI;
            triple.predictedURI = predictedURI;

            db.SubmitChanges();
        }
    }
}