﻿namespace WatchfulEyE
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
     using Database;

    /// <summary>
    /// Summary description for TwitterStatusesAdapter
    /// </summary>
    public class TwitterStatusesAdapter
    {
       private DBEntityDataContext db = new DBEntityDataContext();

        public TwitterStatusesAdapter()
        {
        }

        public List<TwitterStatuse> GetAllTwitterStatuse()
        {
            var statusesQuery = (from s in db.TwitterStatuses
                                 select s);
            List<TwitterStatuse> statuses = new List<TwitterStatuse>();
            statuses = statusesQuery.ToList<TwitterStatuse>();
            return statuses;
        }

        public TwitterStatuse GetTwitterStatuse(long statuseId)
        {
            TwitterStatuse statuse = (from s in db.TwitterStatuses
                                      where s.statuseId == statuseId
                                      select s).First();
            return statuse;
        }


        public void SetTwitterStatuse(TwitterStatuse statuse)
        {
            var q = (from s in db.TwitterStatuses
                     where s.statuseId == statuse.statuseId
                     select s);

            if (q.Count() == 0)
            {
                db.TwitterStatuses.InsertOnSubmit(statuse);
                db.SubmitChanges();
            }
        }
    }
}