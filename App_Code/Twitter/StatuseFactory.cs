﻿namespace WatchfulEyE
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using Database;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Summary description for StatuseFactory
    /// </summary>
    public class StatuseFactory
    {
        TweetsFetcher fetcher;

        public StatuseFactory(string q, string geocode, string since,
            string until, string result_type, string count, string lang)
        {

            fetcher = new TweetsFetcher(q, geocode, since, until, result_type, count, lang);
        }

        public List<TwitterStatuse> Fact()
        {
            List<TwitterStatuse> statuses = new List<TwitterStatuse>();

            string data = fetcher.Fetch();

            JObject jsonData = JObject.Parse(data);
            JArray array = (JArray)jsonData["statuses"];

            for (int x = 0; x < array.Count; x++)
            {
                TwitterStatuse status = new TwitterStatuse();

                JToken token = array[x];

                status.statuseId = token.SelectToken("id").Value<long>();
                status.tweet = token.SelectToken("text").ToString();
                status.createdDate = FixDate(token.SelectToken("created_at").ToString());
                JToken tokenPlace = token.SelectToken("coordinates");

                if (tokenPlace.SelectToken("coordinates") != null)
                {
                    status.longitude = tokenPlace.SelectToken("coordinates")[0].Value<float>();
                    status.latitude = tokenPlace.SelectToken("coordinates")[1].Value<float>();
                }

                statuses.Add(status);
            }
            return statuses;
        }

        private DateTime FixDate(string twitterDate)
        {
            string[] str = twitterDate.Split(' ');
            DateTime date = DateTime.Parse(str[2] + "/" + str[1] + "/" + str[5]
                + " " + str[3]);
            return date;
        }
    }
}