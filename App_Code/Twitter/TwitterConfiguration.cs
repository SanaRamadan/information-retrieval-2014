﻿namespace WatchfulEyE
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Text;
    //using ma

    /// <summary>
    /// Summary description for TwitterConfiguration
    /// </summary>
    public class TwitterConfiguration
    {

        private string consumerKey;
        private string consumerSecret;
        private string accessToken;
        private string accessTokenSecret;

        private string signatureMethod;
        private string version;
        private string nonce;
        private string timeStamp;


        public string ConsumerKey
        {
            get
            {
                consumerKey = "d1PEI2BRUWDF8bg5QBWDQ";
                return consumerKey;
            }
            set { consumerKey = value; }
        }

        public string ConsumerSecret
        {
            get
            {
                consumerSecret = "k8lBmMKfzPzGv1UyEvklkYJx1A8mly2iThhnxCJqg";
                return consumerSecret;
            }
            set { consumerSecret = value; }
        }

        public string AccessToken
        {
            get
            {
                accessToken = "2385725312-2G53tjelWKUtTFB1ACBdwEXxlJPes9HrnV9TBNl";
                return accessToken;
            }
            set { accessToken = value; }
        }

        public string AccessTokenSecret
        {
            get
            {
                accessTokenSecret = "2NOIzvBbCmM0No7cChBzy1n8hUuDaSub31CAgQXr4fGOk";
                return accessTokenSecret;
            }
            set { accessTokenSecret = value; }
        }
        public string SignatureMethod
        {
            get
            {
                signatureMethod = "HMAC-SHA1";
                return signatureMethod;
            }
            set { signatureMethod = value; }
        }

        public string Version
        {
            get
            {
                version = "1.0";
                return version;
            }
            set { version = value; }
        }

        public string Nonce
        {
            get
            {
                nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
                return nonce;
            }
            set { nonce = value; }
        }

        public string TimeStamp
        {
            get
            {
                TimeSpan timeSpan = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
                timeStamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();
                return timeStamp;
            }
            set { timeStamp = value; }
        }

        public TwitterConfiguration() { }

        public TwitterConfiguration(string consumerKey, string consumerSecret, string accessToken, string accessTokenSecret,
         string signatureMethod, string version, string nonce, string timeStamp)
        {
            this.consumerKey = consumerKey;
            this.consumerSecret = consumerSecret;
            this.accessToken = accessToken;
            this.accessTokenSecret = accessTokenSecret;
            this.signatureMethod = signatureMethod;
            this.version = version;
            this.nonce = nonce;
            this.timeStamp = timeStamp;
        }
    }
}