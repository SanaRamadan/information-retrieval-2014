﻿namespace WatchfulEyE
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Net;
    using System.IO;
    using System.Text;
    using System.Security.Cryptography;

    /// <summary>
    /// Summary description for TweetsFetcher
    /// </summary>
    public class TweetsFetcher
    {
        private TwitterConfiguration config;
        private string q;
        private string geocode;
        private string since;
        private string until;
        private string result_type;
        private string count;
        private string lang;

        public TweetsFetcher(string q, string geocode, string since,
            string until, string result_type, string count, string lang)
        {
            this.config = new TwitterConfiguration();
            this.q = q;
            this.geocode = geocode;
            this.since = since;
            this.until = until;
            this.result_type = result_type;
            this.count = count;
            this.lang = lang;
        }

        public string Fetch()
        {
            string dataFromServer = Response(Request());
            return dataFromServer;
        }
        private string QueryFactor()
        {
            string query = "?";
            query += "q=" + q;// +"since%3A" + since;
            query += "&" + "geocode=" + geocode;
            query += "&" + "lang=" + lang;
            query += "&" + "result_type=" + result_type;
            query += "&" + "count=" + count;
     //       if (until != "")
      //          query += "&" + "until=" + until;

            return query;

        }


        private HttpWebRequest Request()
        {
            string url = "https://api.twitter.com/1.1/search/tweets.json" + QueryFactor();
            string oauthconsumerkey = config.ConsumerKey;
            string oauthtoken = config.AccessToken;
            string oauthconsumersecret = config.ConsumerSecret;
            string oauthtokensecret = config.AccessTokenSecret;
            string oauthsignaturemethod = config.SignatureMethod;
            string oauthversion = config.Version;
            string oauthnonce = config.Nonce;
            string oauthtimestamp = config.TimeStamp;

            SortedDictionary<string, string> basestringParameters = new SortedDictionary<string, string>();

            basestringParameters.Add("q", q);//+ "since%3A" + since);
            basestringParameters.Add("geocode", geocode);
            basestringParameters.Add("result_type", result_type);
            //    basestringParameters.Add("until", until);
            basestringParameters.Add("lang", lang);
            basestringParameters.Add("count", count);

            basestringParameters.Add("oauth_version", oauthversion);
            basestringParameters.Add("oauth_consumer_key", oauthconsumerkey);
            basestringParameters.Add("oauth_nonce", oauthnonce);
            basestringParameters.Add("oauth_signature_method", oauthsignaturemethod);
            basestringParameters.Add("oauth_timestamp", oauthtimestamp);
            basestringParameters.Add("oauth_token", oauthtoken);

            //Build the signature string
            string baseString = String.Empty;
            baseString += "GET" + "&";
            baseString += Uri.EscapeDataString(url.Split('?')[0]) + "&";
            foreach (KeyValuePair<string, string> entry in basestringParameters)
            {
                baseString += Uri.EscapeDataString(entry.Key + "=" + entry.Value + "&");
            }

            //Remove the trailing ambersand char last 3 chars - %26
            baseString = baseString.Substring(0, baseString.Length - 3);

            //Build the signing key
            string signingKey = Uri.EscapeDataString(oauthconsumersecret) + "&" + Uri.EscapeDataString(oauthtokensecret);

            //Sign the request
            HMACSHA1 hasher = new HMACSHA1(new ASCIIEncoding().GetBytes(signingKey));
            string oauthsignature = Convert.ToBase64String(hasher.ComputeHash(new ASCIIEncoding().GetBytes(baseString)));

            //Tell Twitter we don't do the 100 continue thing
            ServicePointManager.Expect100Continue = false;

            //authorization header
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(@url);
            string authorizationHeaderParams = String.Empty;
            authorizationHeaderParams += "OAuth ";
            authorizationHeaderParams += "oauth_nonce=" + "\"" + Uri.EscapeDataString(oauthnonce) + "\",";
            authorizationHeaderParams += "oauth_signature_method=" + "\"" + Uri.EscapeDataString(oauthsignaturemethod) + "\",";
            authorizationHeaderParams += "oauth_timestamp=" + "\"" + Uri.EscapeDataString(oauthtimestamp) + "\",";
            authorizationHeaderParams += "oauth_consumer_key=" + "\"" + Uri.EscapeDataString(oauthconsumerkey) + "\",";
            authorizationHeaderParams += "oauth_token=" + "\"" + Uri.EscapeDataString(oauthtoken) + "\",";
            authorizationHeaderParams += "oauth_signature=" + "\"" + Uri.EscapeDataString(oauthsignature) + "\",";
            authorizationHeaderParams += "oauth_version=" + "\"" + Uri.EscapeDataString(oauthversion) + "\"";
            webRequest.Headers.Add("Authorization", authorizationHeaderParams);

            webRequest.Method = "GET";
            webRequest.ContentType = "application/x-www-form-urlencoded";

            //Allow us a reasonable timeout in case Twitter's busy
            webRequest.Timeout = 3 * 60 * 1000;

            return webRequest;
        }

        private string Response(HttpWebRequest webRequest)
        {
            try
            {
                //Proxy settings
                //webRequest.Proxy = new WebProxy("enter proxy details/address");
                HttpWebResponse webResponse = webRequest.GetResponse() as HttpWebResponse;
                Stream dataStream = webResponse.GetResponseStream();

                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);

                // Read the content.
                string responseFromServer = reader.ReadToEnd();
                return responseFromServer;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}