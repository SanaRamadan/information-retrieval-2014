﻿
namespace WatchfulEyE
{
    using System;
    using System.Collections.Generic;
    using edu.stanford.nlp.parser.lexparser;
    using edu.stanford.nlp.process;
    using edu.stanford.nlp.trees;
    using java.io;
   // using Word;


    public class StanfordDependencies
    {
        private static LexicalizedParser lp = LexicalizedParser.loadModel("englishPCFG.ser.gz");

        static public List<SPO> getTriples(string text)
        {
            TreebankLanguagePack tlp = new PennTreebankLanguagePack();
            var tokens = tlp.getTokenizerFactory().getTokenizer(new StringReader(text)).tokenize();
            Tree parse = lp.apply(tokens);

            GrammaticalStructure gs = tlp.grammaticalStructureFactory().newGrammaticalStructure(parse);
            var td = gs.typedDependenciesCCprocessed();

            List<SPO> SPOsentences = new List<SPO>();
            List<Sentence> sl = new List<Sentence>();

            foreach (TypedDependency typedDependency in td.toArray())
            {
                if (typedDependency.reln().toString().Equals("nsubj"))
                {
                    sl.Add(new Sentence(typedDependency.dep().toString(), typedDependency.gov().toString()));
                }
                else if (typedDependency.reln().toString().Equals("rcmod"))
                {
                    sl.Add(new Sentence(typedDependency.gov().toString(), typedDependency.dep().toString()));
                }
                else if (typedDependency.reln().toString().Equals("dobj"))
                {
                    foreach (Sentence sentence in sl)
                    {
                        if (sentence.getAction().Equals(typedDependency.gov().toString()))
                        {
                            Morphology M = new Morphology();
                            String S1 = M.stem(sentence.getAction().Substring(0, sentence.getAction().LastIndexOf('-')));
                            String S2 = sentence.getSubject().Substring(0, sentence.getSubject().LastIndexOf('-'));
                            String S3 = typedDependency.dep().toString().Substring(0, typedDependency.dep().toString().LastIndexOf('-'));
                            SPOsentences.Add(new SPO(S2, S1, S3).processinvalidCharacter());
                        }
                    }
                }
                else if (typedDependency.reln().toString().Contains("prep_"))
                {
                    foreach (Sentence sentence in sl)
                    {
                        if (sentence.getAction().Equals(typedDependency.gov().toString()))
                        {
                            Morphology M = new Morphology();
                            String S1 = M.stem(sentence.getAction().Substring(0, sentence.getAction().LastIndexOf('-')));
                            String S2 = sentence.getSubject().Substring(0, sentence.getSubject().LastIndexOf('-'));
                            String S3 = typedDependency.dep().toString().Substring(0, typedDependency.dep().toString().LastIndexOf('-'));
                            SPOsentences.Add(new SPO(S2, S1, S3).processinvalidCharacter());
                        }
                    }
                }
                else if (typedDependency.reln().toString().Equals("cop"))
                {
                    foreach (Sentence sentence in sl)
                    {
                        if (sentence.getAction().Equals(typedDependency.gov().toString()))
                        {
                            Morphology M = new Morphology();
                            String S1 = M.stem(typedDependency.dep().toString().Substring(0, typedDependency.dep().toString().LastIndexOf('-')));
                            String S2 = sentence.getSubject().Substring(0, sentence.getSubject().LastIndexOf('-'));
                            String S3 = sentence.getAction().Substring(0, sentence.getAction().LastIndexOf('-'));
                            SPOsentences.Add(new SPO(S2, S1, S3).processinvalidCharacter());
                        }
                    }
                }
                else if (typedDependency.reln().toString().Equals("nsubjpass"))
                {
                    sl.Add(new Sentence(typedDependency.dep().toString(), typedDependency.gov().toString()));
                }
                else if (typedDependency.reln().toString().Equals("auxpass"))
                {

                    foreach (Sentence sentence in sl)
                    {
                        if (sentence.getAction().Equals(typedDependency.gov().toString()))
                        {
                            Morphology M = new Morphology();
                            String S1 = M.stem(sentence.getAction().Substring(0, sentence.getAction().LastIndexOf('-')));
                            String S2 = sentence.getSubject().Substring(0, sentence.getSubject().LastIndexOf('-'));
                            String S3 = typedDependency.dep().toString().Substring(0, typedDependency.dep().toString().LastIndexOf('-'));
                            SPOsentences.Add(new SPO(S2, S1, S3).processinvalidCharacter());
                        }
                    }
                }
            }
            return SPOsentences;
        }

        private class Sentence
        {
            private String Action = "", subject = "";

            public String getAction()
            {
                return Action;
            }

            public String getSubject()
            {
                return subject;
            }

            public Sentence(String sub, String act)
            {
                this.Action = act;
                this.subject = sub;

            }
        }
    }
}
