﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SlangToEng
/// </summary>
public class SlangToEng
{
	public SlangToEng()
	{
	}
    public static string LookUpTable(string Tweet)
    {
        foreach (string word in Tweet.Split(' '))
	    {
            if (word.Equals("!"))
            {
                word.Replace("!", "I have a comment");
            }
            else if (word.Equals("**//"))
            {
                word.Replace("**//", "i wink you");   
            }
            else if (word.Equals("10Q"))
            {
                word.Replace("10Q", "Thank you");
            }
            else if (word.Equals(".S"))
            {
                word.Replace(".S", "I am feeling sick");
            }
            else if (word.Equals("121"))
            {
                word.Replace("121", "one to one");
            }
            else if (word.Equals("143"))
            {
                word.Replace("143", "i love you");
            }
            else if (word.Equals("1432"))
            {
                word.Replace("1432", "i love you too");
            }
            else if (word.Equals("14AA41"))
            {
                word.Replace("14AA41", "One for All and All for One");
            }
            else if (word.Equals("182"))
            {
                word.Replace("182", "I hate you");
            }
            else if (word.Equals("1daful"))
            {
                word.Replace("1daful", "wonderful");
            }
            else if (word.Equals("2"))
            {
                word.Replace("2", "to, too, two");
            }
            else if (word.Equals("24/7"))
            {
                word.Replace("24/7", "In all the time");
            }
            else if (word.Equals("2b"))
            {
                word.Replace("2b", "To be");
            }
            else if (word.Equals("2B or not 2B"))
            {
                word.Replace("2b", "To Be Or Not To Be");
            }
            else if (word.Equals("2b or not 2b"))
            {
                word.Replace("2b", "To Be Or Not To Be");
            }
            else if (word.Equals("2b@"))
            {
                word.Replace("2b@", "To Be at");
            }
            else if (word.Equals("2BZ4UQT"))
            {
                word.Replace("2BZ4UQT", "Too Busy For You Cutey");
            }
            else if (word.Equals("2B~not2B"))
            {
                word.Replace("2B~not2B", "To be or not to be");
            }
            else if (word.Equals("2d4"))
            {
                word.Replace("2d4", "To die for");
            }
            else if (word.Equals("2day"))
            {
                word.Replace("2day", "Today");
            }
            else if (word.Equals("2G2B4G"))
            {
                word.Replace("2G2B4G", "Too Good To Be Forgotten");
            }
            else if (word.Equals("2G2B4G"))
            {
                word.Replace("2G2B4G", "Too Good To Be Forgotten");
            }
            else if (word.Equals("2G2BT"))
            {
                word.Replace("2G2BT", "Too Good To Be True");
            }
            else if (word.Equals("2moro"))
            {
                word.Replace("2moro", "Tomorrow");
            }
            else if (word.Equals("2nite"))
            {
                word.Replace("2nite", "Tonight");
            }
            else if (word.Equals("2QT"))
            {
                word.Replace("2QT", "Too Cute");
            }
            else if (word.Equals("2U2"))
            {
                word.Replace("2U2", "To You Too");
            }
            else if (word.Equals("303"))
            {
                word.Replace("303", "Mom");
            }
            else if (word.Equals("4eva"))
            {
                word.Replace("4eva", "Forever");
            }
            else if (word.Equals("4ever"))
            {
                word.Replace("4ever", "Forever");
            }
            else if (word.Equals("<3"))
            {
                word.Replace("<3", "i love you");
            }
            else if (word.Equals("?"))
            {
                word.Replace("?", "i have a question");
            }
            else if (word.Equals("@TEOTD"))
            {
                word.Replace("@TEOTD", "At The End Of The Day");
            }
            else if (word.Equals("A2D"))
            {
                word.Replace("A2D", "Agree to Disagree");
            }
            else if (word.Equals("abt"))
            {
                word.Replace("abt", "about");
            }
            else if (word.Equals("^URS"))
            {
                word.Replace("^URS", "Up Yours");
            }
            else if (word.Equals("^RUP^"))
            {
                word.Replace("^RUP^", "Read Up Please");
            }
            else if (word.Equals("ZZZ"))
            {
                word.Replace("ZZZ", "Sleeping, Tired");
            }
            else if (word.Equals("OMG"))
            {
                word.Replace("OMG", "Oh my god");
            }
                //Twitter Slang
            else if (word.Equals("@reply"))
            {
                word.Replace("@reply", "update");
            }
            else if (word.Equals("ab"))
            {
                word.Replace("ab", "about");
            }
            else if (word.Equals("abt"))
            {
                word.Replace("abt", "about");
            }
            else if (word.Equals("attwaction"))
            {
                word.Replace("attwaction", "attraction between two users");
            }
            else if (word.Equals("b/c"))
            {
                word.Replace("b/c", "because");
            }
            else if (word.Equals("B"))
            {
                word.Replace("B", "be");
            }
            else if (word.Equals("b4"))
            {
                word.Replace("b4", "before");
            }
            else if (word.Equals("BFN"))
            {
                word.Replace("BFN", "bye for now");
            }
            else if (word.Equals("bgd"))
            {
                word.Replace("bgd", "background");
            }
            else if (word.Equals("BR"))
            {
                word.Replace("BR", "best regards");
            }
            else if (word.Equals("chk"))
            {
                word.Replace("chk", "check");
            }
            else if (word.Equals("cld"))
            {
                word.Replace("cld", "could");
            }
            else if (word.Equals("clk"))
            {
                word.Replace("clk", "click");
            }
            else if (word.Equals("co-twitterer"))
            {
                word.Replace("co-twitterer", "Twitter account");
            }
            else if (word.Equals("cre8"))
            {
                word.Replace("cre8", "create");
            }
            else if (word.Equals("da"))
            {
                word.Replace("da", "the");
            }
            else if (word.Equals("deets"))
            {
                word.Replace("deets", "details");
            }
            else if (word.Equals("detweet"))
            {
                word.Replace("detweet", "deleted tweet");
            }
            else if (word.Equals("Dweet"))
            {
                word.Replace("Dweet", "drunk user");
            }
            else if (word.Equals("EM"))
            {
                word.Replace("EM", "e-mail");
            }
            else if (word.Equals("eml"))
            {
                word.Replace("eml", "e-mail");
            }
            else if (word.Equals("EMA"))
            {
                word.Replace("EMA", "e-mail");
            }
            else if (word.Equals("F2F"))
            {
                word.Replace("F2F", "face to face");
            }
            else if (word.Equals("fab"))
            {
                word.Replace("fab", "fabulous");
            }
            else if (word.Equals("FTL"))
            {
                word.Replace("FTL", "for the loss");
            }
            else if (word.Equals("FTW"))
            {
                word.Replace("FTW", "for the win");
            }
            else if (word.Equals("IC"))
            {
                word.Replace("IC", "I see");
            }
            else if (word.Equals("ICYMI"))
            {
                word.Replace("ICYMI", "in case you missed it");
            }
            else if (word.Equals("idk"))
            {
                word.Replace("idk", "I don’t know");
            }
            else if (word.Equals("kk"))
            {
                word.Replace("kk", "cool cool");
            }
            else if (word.Equals("NTS"))
            {
                word.Replace("NTS", "note to self");
            }
            else if (word.Equals("OH"))
            {
                word.Replace("OH", "overheard");
            }
            else if (word.Equals("PRT"))
            {
                word.Replace("PRT", "please retweet");
            }
            else if (word.Equals("TMB"))
            {
                word.Replace("TMB", "Tweet me back");
            }
            else if (word.Equals("Twaffic"))
            {
                word.Replace("Twaffic", "Twitter traffic");
            }
            else if (word.Equals("Twalking"))
            {
                word.Replace("Twalking", "Twitting while walking");
            }
            else if (word.Equals("Tweeple"))
            {
                word.Replace("Tweeple", "Twitter users");
            }
            else if (word.Equals("tweetorial"))
            {
                word.Replace("tweetorial", "Twitter tutorial");
            }
            else if (word.Equals("twettiquette:"))
            {
                word.Replace("twettiquette:", "acceptable Twitter behavior");
            }
            else if (word.Equals("Twishing"))
            {
                word.Replace("Twishing", "Twitter and phishing");
            }
            else if (word.Equals("Twitosphere"))
            {
                word.Replace("Twitosphere", "World of Twitter");
            }
            else if (word.Equals("U"))
            {
                word.Replace("U", "you");
            }
            else if (word.Equals("woz"))
            {
                word.Replace("woz", "was");
            }
            else if (word.Equals("wtv"))
            {
                word.Replace("wtv", "whatever");
            }
            else if (word.Equals("ykyat"))
            {
                word.Replace("ykyat", "you know you’re addicted to");
            }
            else if (word.Equals("yoyo"))
            {
                word.Replace("yoyo", "you’re on your own");
            }
            else if (word.Equals("ztwitt"))
            {
                word.Replace("ztwitt", "to tweet extremely fast");
            }
            else if (word.Equals("2EZ"))
            {
                word.Replace("2EZ", "Too easy");
            }
            else if (word.Equals("2G2BT"))
            {
                word.Replace("2G2BT", "Too good to be true");
            }
            else if (word.Equals("2M2H"))
            {
                word.Replace("2M2H", "Too much too handle");
            }
            else if (word.Equals("2MI"))
            {
                word.Replace("2MI", "Too much information");
            }
            else if (word.Equals("2MOR"))
            {
                word.Replace("2MOR", "Tomorrow");
            }
            else if (word.Equals("2NTE"))
            {
                word.Replace("2NTE", "Tonight");
            }
            else if (word.Equals("2NTE"))
            {
                word.Replace("2NTE", "Tonight");
            }
            else if (word.Equals("404"))
            {
                word.Replace("404", "I don't know");
            }
            else if (word.Equals("88"))
            {
                word.Replace("88", "Bye-bye");
            }
            else if (word.Equals("ACC"))
            {
                word.Replace("ACC", "Anyone can come");
            }
            else if (word.Equals("ACK"))
            {
                word.Replace("ACK", "Acknowledge");
            }
            else if (word.Equals("AIGHT"))
            {
                word.Replace("AIGHT", "Alright");
            }
            else if (word.Equals("AH"))
            {
                word.Replace("AH", "At home");
            }
            else if (word.Equals("B2B"))
            {
                word.Replace("B2B", "Business-to-business");
            }
            else if (word.Equals("B2C"))
            {
                word.Replace("B2C", "Business-to-consumer");
            }
            else if (word.Equals("B2W"))
            {
                word.Replace("B2W", "Back to work");
            }
            else if (word.Equals("CYAL8R"))
            {
                word.Replace("CYAL8R", "See you later");
            }
            else if (word.Equals("CYE"))
            {
                word.Replace("CYE", "Check your e-mail");
            }
            else if (word.Equals("CYA"))
            {
                word.Replace("CYA", "See you");
            }
            else if (word.Equals("CYA"))
            {
                word.Replace("CYA", "See you");
            }
            else if (word.Equals("FF"))
            {
                word.Replace("FF", "Follow Friday");
            }
            else if (word.Equals("GL"))
            {
                word.Replace("GL", "Good luck");
            }
            else if (word.Equals("GJ"))
            {
                word.Replace("GJ", "Good job");
            }
            else if (word.Equals("GNIGHT"))
            {
                word.Replace("GNIGHT", "Good night");
            }
            else if (word.Equals("GNITE"))
            {
                word.Replace("GNITE", "Good night");
            }
            else if (word.Equals("K"))
            {
                word.Replace("K", "Okay");
            }
            else if (word.Equals("R"))
            {
                word.Replace("R", "are");
            }
            else if (word.Equals("RIP"))
            {
                word.Replace("RIP", "Rest in peace");
            }
            else if (word.Equals("RLY"))
            {
                word.Replace("RLY", "Really");
            }
            else if (word.Equals("RT"))
            {
                word.Replace("RT", "Retweet ");
            }
            else if (word.Equals("RU"))
            {
                word.Replace("RU", "Are you? ");
            }
            else if (word.Equals("RUT"))
            {
                word.Replace("RUT", "Are you there?");
            }
            else if (word.Equals("RUOK"))
            {
                word.Replace("RUOK", "Are you okay?");
            }
            else if (word.Equals("RW"))
            {
                word.Replace("RW", "Real world");
            }
            else if (word.Equals("SH^"))
            {
                word.Replace("SH^", "Shut up");
            }
            else if (word.Equals("SOMY"))
            {
                word.Replace("SOMY", "Sick of me yet?");
            }
            else if (word.Equals("VN"))
            {
                word.Replace("VN", "Very nice");
            }
            else if (word.Equals("WRK"))
            {
                word.Replace("WRK", "Work");
            }
            else if (word.Equals("Y"))
            {
                word.Replace("Y", "Why?");
            }
            else if (word.Equals("YBS"))
            {
                word.Replace("YBS", "You'll be sorry");
            }
            else if (word.Equals("ZUP"))
            {
                word.Replace("ZUP", "What'a up?");
            }
            else if (word.Equals("7K"))
            {
                word.Replace("7K", "Sick");
            }
            else if (word.Equals(":("))
            {
                word.Replace(":(", "sad");
            }
            else if (word.Equals("sic"))
            {
                word.Replace("sic", "sick");
            }
            else if (word.Equals("A3"))
            {
                word.Replace("A3", "Anyplace, anywhere, anytime");
            }
            else if (word.Equals("ITIGBS"))
            {
                word.Replace("ITIGBS", "I think I'm going to be sick");
            }
                //symbols
            else if (word.Equals("☹"))
            {
                word.Replace("☹", "I am sad");
            }
            else if (word.Equals("☤"))
            {
                word.Replace("☤", "I am sick and taking drugs");
            }
            else if (word.Equals(":-&"))
            {
                word.Replace(":-&", "I am sick");
            }
            else if (word.Equals("+o("))
            {
                word.Replace("+o(", "I am sick");
            }
            else if (word.Equals("(-__-)"))
            {
                word.Replace("(-__-)", "sick, exhausted");
            }
            else if (word.Equals(":->|"))
            {
                word.Replace(":->|", "flu disease");
            }
            else if (word.Equals("[ ± _ ± ]"))
            {
                word.Replace("[ ± _ ± ]", "tired");
            }
            else if (word.Equals("%')"))
            {
                word.Replace("%')", "very tired");
            }
          }
        return Tweet;
    }

    public static string WhiteList(string Tweet)
    {
        string EngTweet =LookUpTable(Tweet);
        return EngTweet;
    }
	
}