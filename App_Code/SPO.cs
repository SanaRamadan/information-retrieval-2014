﻿namespace WatchfulEyE
{
    using System;
    using System.Text.RegularExpressions;

    /// <summary>
    /// Summary description for SPO
    /// </summary>
    public class SPO
    {
        private String subject;
        private String predicted;
        private String objectt;

        public SPO(String subject, String predicted, String objectt)
        {
            this.subject = subject;
            this.predicted = predicted;
            this.objectt = objectt;
        }
        public String Subject
        {
            get { return subject; }
            set { subject = value; }
        }

        public String Predicted
        {
            get { return predicted; }
            set { predicted = value; }
        }

        public String Object
        {
            get { return objectt; }
            set { objectt = value; }
        }

        public override string ToString()
        {
            return "subject is " + Subject + ", verb is " + Predicted + ", Object is " + Object + ".";
        }

        public bool equal(SPO tripl1, SPO tripl2)
        {
            if ((tripl1.Subject == tripl2.Subject)
                && (tripl1.Predicted == tripl2.Predicted)
                && (tripl1.Object == tripl2.Object))
            {
                return true;
            }
            else
                return false;
        }
        public SPO processinvalidCharacter()
        {
            this.subject = Regex.Replace(this.subject, "[! \" \'  , . : ?]", "");
            this.objectt = Regex.Replace(this.objectt, "[! \" \'  , . : ?]", "");
            this.predicted = Regex.Replace(this.predicted, "[! \" \'  , . : ?]", "");
            return new SPO(this.subject, this.predicted, this.objectt);
        }
    }
}

