﻿namespace WatchfulEyE
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using Database;
    using VDS.RDF;
    using VDS.RDF.Parsing;
    using VDS.RDF.Parsing.Validation;

    /// <summary>
    /// Summary description for RDFCreator
    /// </summary>
    public class RDFCreator
    {
        public static string rdf_ns = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
        public static string we_ns = "http://www.watchfuleye.com/resource/";
        public static string property_ns = "http://www.watchfuleye.com/property#";

        public static string rdf = "rdf";
        public static string we = "we";
        public static string property = "property";

        public static string longitude = "longitude";
        public static string latitude = "latitude";
        public static string createdDate = "createdDate";

        private string rdf_store = "";

        public RDFCreator() { }

        public void Create(List<Database.Triple> triples)
        {
            try
            {
                FileStream aFile = new FileStream(@"../../Output/myRDF.rdf", FileMode.Create);
                StreamWriter sw = new StreamWriter(aFile);

                string temp = "<?xml version=\"1.0\"?>\n"
                      + "<" + rdf + ":RDF\n"

                      + "xmlns:" + rdf + "=\"" + rdf_ns + "\"\n"
                      + "xmlns:" + property + "=\"" + property_ns + "\"\n"
                      + "xmlns:" + we + "=\"" + we_ns + "\">\n";

                rdf_store += temp;
                sw.WriteLine(temp);

                foreach (Database.Triple triple in triples)
                {
                    temp = "<" + rdf + ":Description rdf:about=\"" + triple.subjectURI + "\">\n"
                    + "<" + we + ":" + triple.predicted + " rdf:resource=\"" + triple.objectURI + "\"/>\n"
                    + "<" + property + ":" + longitude + ">" + 2.15
                    + "</" + property + ":" + longitude + ">\n"
                    + "<" + property + ":" + latitude + ">" + 75.36
                    + "</" + property + ":" + latitude + ">\n"
                    + "<" + property + ":" + createdDate
                    + " rdf:datatype=\"http://www.w3.org/2001/XMLSchema#dateTime\">"
                    + "3-5-2013" + "</" + property + ":" + createdDate + ">\n"
                    + "</" + rdf + ":Description>\n";

                    rdf_store += temp;
                    sw.WriteLine(temp);
                }

                temp = "</" + rdf + ":RDF>\n";

                rdf_store += temp;
                sw.WriteLine(temp);
                sw.Close();
            }

            catch (System.IO.IOException e)
            {
                System.Console.WriteLine("IOException");
            }
        }

        public bool Validate()
        {
            IRdfReader parse = new RdfXmlParser(RdfXmlParserMode.Streaming);
            RdfStrictSyntaxValidator s = new RdfStrictSyntaxValidator(parse);
            ISyntaxValidationResults validate = s.Validate(rdf_store);
            return validate.IsValid;
        }
    }
}